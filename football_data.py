import http.client
import json
from pprint import pprint
import requests
import csv

token = 'efda008899e346f693efa9c75f9577ee'
connection = http.client.HTTPConnection('api.football-data.org')
headers = { 'X-Auth-Token': token, 'X-Response-Control': 'minified' }


def get_games_by_league_id(Idleague):
    connection.request('GET', '/v1/competitions/' + str(Idleague) + '/fixtures', None, headers )
    wdate = json.loads(connection.getresponse().read().decode())
    return wdate

def get_standings_by_league_id(Idleague):
    connection.request('GET', '/v1/competitions/' + str(Idleague) + '/leagueTable', None, headers )
    wdate = json.loads(connection.getresponse().read().decode())
    return wdate


def get_players_by_team_id(Idteam):
    connection.request('GET', '/v1/teams/' + str(Idteam) + '/players', None, headers )
    wdate = json.loads(connection.getresponse().read().decode())
    return wdate

def get_players_by_team_id(Idteam):
    connection.request('GET', '/v1/teams/' + str(Idteam) + '/players', None, headers )
    wdate = json.loads(connection.getresponse().read().decode())
    return wdate

def get_id_competion_by_year_and_code(year, code):
    connection.request('GET', '/v1/competitions/?season=' + str(year), None, headers )
    wdate = json.loads(connection.getresponse().read().decode())
    for c in wdate:
        if c['league'] == code:
            return c['id'] 
   




def main():
    #print(get_standings_by_league_id(get_id_competion_by_year_and_code(2017, 'PL')))
    #print(get_players_by_team_id(754))
    #print()
    
    connection = http.client.HTTPConnection('api.football-data.org')
    headers = { 'X-Auth-Token': token, 'X-Response-Control': 'minified' }
    connection.request('GET', '/v1/competitions/?season=2016', None, headers )
    wdate = json.loads(connection.getresponse().read().decode())
    csvfile = open('tournaments.csv', 'w', newline='', encoding='utf-8')
    csvwriter = csv.writer(csvfile,  delimiter=',')
    a = []
    for i in range(len(wdate)):
        b = []
        b.append(wdate[i]["id"])
        b.append(wdate[i]["caption"])
        b.append(wdate[i]["league"])
        csvwriter.writerow(b) 
        a.append(b)
 
    connection.request('GET', '/v1/competitions/426/leagueTable', None, headers )
    wdate = json.loads(connection.getresponse().read().decode())
    print(wdate)
    csvfile = open('teams.csv', 'w', newline='', encoding='utf-8')
    csvwriter = csv.writer(csvfile,  delimiter=',')
 
    try:
        wdate = wdate["standing"]
    except:
        wdate = wdate["standings"]   


    teamID = []
    teamName = []
    teamPic = []

    for i in range(len(wdate)):
        b = []
        teamID.append(wdate[i]["teamId"])
        teamName.append(wdate[i]["team"])
        teamPic.append(wdate[i]["crestURI"])
        b.append(wdate[i]["teamId"])
        b.append(wdate[i]["team"])
        b.append(wdate[i]["rank"])
        b.append(wdate[i]["points"])
        csvwriter.writerow(b) 
        a.append(b)


    csvfile = open('players.csv', 'w', newline='', encoding='utf-8')
    csvwriter = csv.writer(csvfile,  delimiter=',')

    for i in range(len(teamID)):
        req = '/v1/teams/' + str(teamID[i]) + '/players'
        connection.request('GET', req, None, headers )
        wdate = json.loads(connection.getresponse().read().decode())
        wdate = wdate["players"]
        for j in range(len(wdate)):
            b = []
            b.append(wdate[j]["name"])
            b.append(wdate[j]["nationality"])
            b.append(wdate[j]["position"])
            b.append(wdate[j]["marketValue"])
            b.append(wdate[j]["dateOfBirth"])
            b.append(teamPic[i])
            b.append(teamName[i])
            csvwriter.writerow(b) 
    
main()     
